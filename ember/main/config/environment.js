/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'ember-firestarter',
    environment: environment,
    contentSecurityPolicy: { 'connect-src': "'self' https://auth.firebase.com wss://*.firebaseio.com" },
    firebase: 'https://ember-firestarter.firebaseio.com/',
    baseURL: '/',
    locationType: 'auto',
    googleMapsAPIKey: 'AIzaSyAkjyCxpz7Jn5OiTnWrS1cepreUVpHBdvY',
    geolocationAPI: 'AIzaSyChoOy9RGyLEVJqV1RQB08CwIATxi8be9I',
    googleFonts: [
      'Open+Sans:300,400,500,600,700'
    ],
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };
  ENV.contentSecurityPolicy = {
    'default-src': "'none'",
    'script-src': "'self' 'unsafe-eval' *.googleapis.com maps.gstatic.com",
    'font-src': "'self' fonts.gstatic.com",
    'connect-src': "'self' maps.gstatic.com https://auth.firebase.com wss://*.firebaseio.com",
    'img-src': "'self' *.googleapis.com maps.gstatic.com csi.gstatic.com data: s3-eu-west-1.amazonaws.com",
    'style-src': "'self' 'unsafe-inline' fonts.googleapis.com maps.gstatic.com"
  };

  
  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.backendPath = 'http://mujvuz.herokuapp.com';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.baseURL = '/';
    ENV.backendPath = '';
  }

  return ENV;
};
