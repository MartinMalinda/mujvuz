import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('user', function() {
    this.route('signup');
    this.route('login');
  });
  this.route('shop', {path: '/shop/:shop_id'}, function() {
    this.route('order');
  });
  this.route('list', {path: '/list/:city/:brand'});
  this.route('add');
  this.route('jak-to-funguje');
  this.route('o-nas');
  this.route('kontakt');
  this.route('page', {path: '/p/:page_subject'});
});

export default Router;
