import Ember from 'ember';

const {
  computed,
  get,
  isEmpty
} = Ember;


export default function notEmpty(firstKey) {
  return computed(firstKey, {
    get() {
      return !isEmpty(this.get(firstKey));
    }
  });
}