import DS from 'ember-data';
import Ember from 'ember';
// import config from 'ember-firestarter/config/environment';

const {attr, belongsTo} = DS;
const {computed} = Ember;
const {htmlSafe} = Ember.String;

export default DS.Model.extend({

  filename: attr('string'),
  shop: belongsTo('repairShop'),
  note: attr('string'),
  ready: attr('boolean'),
  fullUrl: computed('filename', function(){
    return 'https://s3-eu-west-1.amazonaws.com/mujvuz2/' + this.get('filename');
  }),
  style: computed('fullUrl', function(){
  	return htmlSafe(`background-image:url('${this.get('fullUrl')}')`);
  })

});
