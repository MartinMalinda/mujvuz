import DS from 'ember-data';

const {attr} = DS;

export default DS.Model.extend({
  subject: attr('string'),
  hash: attr('string'),
  html: attr('string')
});
