import DS from 'ember-data';
import Ember from 'ember';

const {computed, observer} = Ember;
const {attr, belongsTo} = DS;

export default DS.Model.extend({
  latitude: attr('number'),
  longitude: attr('number'),

  repairShop: belongsTo('repairShop'),

  markerObject: computed('googleMapObject', function(){
    
    var position = new google.maps.LatLng(this.get('latitude'),this.get('longitude'));
    var marker = new google.maps.Marker({
      position: position,
      title: 'test',
      icon: './images/marker-small.png'
    });



    return marker;
  }),

  // infoWindowObject: computed('markerObject', function(){
   
  //   //return infowindow;
  // }),

  markerInserter: observer('googleMapObject', function(){
  	
    var marker = this.get('markerObject');
  	marker.setMap(this.get('googleMapObject'));
    // this.notifyPropertyChange('infoWindowObject');
  	// console.log(marker);
  	// return marker;
  }).on('init')
});
