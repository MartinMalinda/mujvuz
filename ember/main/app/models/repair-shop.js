import DS from 'ember-data';
import Ember from 'ember';

const {attr, belongsTo, hasMany} = DS;
const {computed, isEmpty} = Ember;

export default DS.Model.extend({
  name: attr('string'),
  address: attr('string'),
  brand: attr('string'),
  city: attr('string'),

  info: attr('string'),

  image: attr('string'),

  phone: attr('string'),
  url: attr('string'),
  email: attr('string'),
  mainImageId: attr('string'),

  isVisible: attr('boolean', {defaultValue: false}),

  announcement: attr('string'),

  mondayStart: attr('string'),
  tuesdayStart: attr('string'),
  wednesdayStart: attr('string'),
  thursdayStart: attr('string'),
  fridayStart: attr('string'),
  saturdayStart: attr('string'),
  sundayStart: attr('string'),

  mondayEnd: attr('string'),
  tuesdayEnd: attr('string'),
  wednesdayEnd: attr('string'),
  thursdayEnd: attr('string'),
  fridayEnd: attr('string'),
  saturdayEnd: attr('string'),
  sundayEnd: attr('string'),

  minElectricPrice: attr('number', {defaultValue: 0}),
  minMechPrice: attr('number', {defaultValue: 0}),
  minKlempPrice: attr('number', {defaultValue: 0}),
  minLakPrice: attr('number', {defaultValue: 0}),

  maxElectricPrice: attr('number', {defaultValue: 0}),
  maxMechPrice: attr('number', {defaultValue: 0}),
  maxKlempPrice: attr('number', {defaultValue: 0}),
  maxLakPrice: attr('number', {defaultValue: 0}),

  cityBrandIndex: attr('string'),

  latitude: attr('number'),
  longitude: attr('number'),

  marker: belongsTo('marker'),
  images: hasMany('image'),
  extra: belongsTo('extra'),

  mainImage: computed('mainImageId', function(){
    var id = this.get('mainImageId');
    if(!isEmpty(id)){
      return this.get('images').findBy('id', id);
    } else {
      return this.get('images.firstObject');
    }
  }),
});
