import Ember from 'ember';

const {computed, on, debug, $} = Ember;

export default Ember.Component.extend({

	googleMapObject : null,
	markers: [],
	isMapBusy: false,
	action: 'initLinks',

	zoom: computed('googleMapObject',{
		get(){
			var gObject = this.get('googleMapObject');
			if(gObject){
				return gObject.getZoom();
			} else {
				return 1;
			}
		},
		set(key, value){
			var gObject = this.get('googleMapObject');
			if(gObject && value){
				// if(value )
				gObject.setZoom(parseInt(value));
			} else {
				// return 1;
			}
		}
	}),
	

	latitude: computed('googleMapObject', {
		get(){
			var gObject = this.get('googleMapObject');
			if (gObject){
				return gObject.getCenter().lat();
			} else {
				return 30;
			}
		},
		set(key, value){
			if(!value) { value = 10; }
			return value;
			// this.set('latitude', value);
		}
	}),

	longitude: computed('googleMapObject', {
		get(){
			var gObject = this.get('googleMapObject');
			if (gObject){
				return gObject.getCenter().lng();
			} else {
				return 30;
			}
		},
		set(key, value){
			if(!value) { value = 10; }
			return value;
		}
	}),

	insertMarkers() {

		const fitBounds = this.get('fitBounds');
		var map = this.get('googleMapObject');
		var bounds;

		if(fitBounds){
			bounds = new google.maps.LatLngBounds();
		}

		this.get('markers').forEach( marker => {
			marker.set('googleMapObject', map);

			if(fitBounds) {
				// console.log(marker.get('markerObject'));
				var latlng = new google.maps.LatLng(marker.get('latitude'),marker.get('longitude'));
				bounds.extend(latlng);
			}
		});

		if(fitBounds){
			map.fitBounds(bounds);
		
		}
	},
	initEvents(){
		var map = this.get('googleMapObject');

		map.addListener('zoom_changed', () => {
			
			this.notifyPropertyChange('zoom');

		});

		map.addListener('center_changed', () => {
			
			this.notifyPropertyChange('latitude');
			this.notifyPropertyChange('longitude');
			
				
		})

	},
	initMap: on('didInsertElement', function(){

		this.destroyGoogleMap();
		var element = $('#' + this.elementId)[0];
		var options = {
			center: new google.maps.LatLng(this.get('latitude'), this.get('longitude')),
			zoom: 1,
			disableDefaultUI: true,
			maxZoom: 16
		};
		var googleMapObject = new google.maps.Map(element, options);
		this.set('googleMapObject', googleMapObject);

		this.insertMarkers();
		this.initEvents();

	}),
  /**
   * Destroy the map
   */
  destroyGoogleMap: on('willDestroyElement', function () {
    if (this.get('googleMapObject')) {
      debug('[google-map] destroying %@');
      this.set('googleMapObject', null);
    }
  }),

  actions: {
  	center(){
		var gObject = this.get('googleMapObject');
		if(gObject){
			var latlng = new google.map.LatLng(this.get('latitude'), this.get('longitude'));
			gObject.panTo(latlng);
		}
	}
  }
});
