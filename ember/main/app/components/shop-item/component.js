import Ember from 'ember';
import notEmpty from 'ember-firestarter/macros/not-empty';


const {computed, isEmpty} = Ember;

export default Ember.Component.extend({
	classNames: ['shop-item'],
	isVisible: computed.alias('model.isVisible'),
	hasImage: notEmpty('model.mainImage.fullUrl'),
	hasAnnouncement: notEmpty('model.announcement')
});
