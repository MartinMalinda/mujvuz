import Ember from 'ember';

const {on, $} = Ember;

export default Ember.Component.extend({
	classNames: ['inline-block'],
	didInit : false,


	slideToggle(){
		let target = $('#bs-example-navbar-collapse-1');
		target.slideToggle();
		let button = $('[data-toggle]');

		if(button.attr('aria-expanded') === 'true'){
			button.attr('aria-expanded', 'false');
		} else {
			button.attr('aria-expanded', 'true');
		}
	},

	slideOff(){
		let target = $('#bs-example-navbar-collapse-1');
		let button = $('[data-toggle]');

		target.slideUp();
		button.attr('aria-expanded', 'false');
	},

	// pathObserver: Ember.observer('currentPath', function(){
	// 	if(this.get('didInit')){
	// 		this.slideOff();
	// 	}
	// }),

	checkVisibility: on('didInsertElement', function(){
		let target = $('#bs-example-navbar-collapse-1');
		let button = $('[data-toggle]');
		target.slideUp();

		if(target.is(':hidden')){
			button.attr('aria-expanded', 'false');
		} else {
			button.attr('aria-expanded', 'true');
		}

		this.set('didInit', true);	
	}),

	didReceiveAttrs(){
		if(this.get('didInit')){
			this.slideOff();
		}
	},
	

	toggleNav: on('click', function(){
		this.slideToggle();
	})
});
