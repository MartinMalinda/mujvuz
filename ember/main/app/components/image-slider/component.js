import Ember from 'ember';

const {computed, run, on} = Ember;

export default Ember.Component.extend({
	// attributeBindings: ['style'],
	classNames: ['box', 'image','slider'],
	classNameBindings: ['movingLeft:left', 'movingRight:right'],

	position:0,
	movingLeft: false,
	movingRight: false,

	moveMainImage: on('init', function(){
		let images = this.get('images');
		let mainImage = this.get('mainImage');

		let shiftedImages = images.without(mainImage);
		console.log(shiftedImages);

		shiftedImages.unshiftObject(mainImage);

		this.set('images', shiftedImages);
	}),

	previousImage: computed('position', function(){
		return this.get('images').objectAt(this.get('position') -1);
		
	}),

	currentImage: computed('position', function(){
		return this.get('images').objectAt(this.get('position'));
	}),

	nextImage: computed('position', function(){
		return this.get('images').objectAt(this.get('position') + 1);
	}),

	actions: {

		slideLeft(){
				
			this.set('movingLeft', true);
			run.debounce(() => {

			this.decrementProperty('position');
			this.set('movingLeft', false);

			}, 200);
		},

		slideRight(){
			this.set('movingRight', true);
			run.debounce(() => {
				this.incrementProperty('position');
				this.set('movingRight', false);
			}, 200);
		}
	}
});
