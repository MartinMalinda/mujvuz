import Ember from 'ember';
import ResetScroll from 'ember-firestarter/mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	model(params){

		const index = `${params.city}_${params.brand}`;
		
		return this.store.query('repairShop', {orderBy: 'cityBrandIndex', startAt: index, endAt: index});
	},
	// afterModel(model){
	// 	// model.forEach(shop => {
	// 	// });
	// }
});
