import Ember from 'ember';

const {inject, computed, $} = Ember;

export default Ember.Controller.extend({

	limit: 7,
	queryParams: ['limit'],

	application: inject.controller('application'),
	// _routing: inject.service('-routing'),
	currentPosition: computed.alias('application.geoposition'),

	visibleMarkers: computed.filterBy('model','isVisible',true),
	shopsSorting: ['marker.distance'],
	sortedShops: computed.sort('model', 'shopsSorting'),
	noShopsAvailible: computed.equal('visibleMarkers.length', 0),
	isGeopositionAllowed: computed.alias('application.geopositionAllowed'),

	limitedShops: computed('sortedShops','limit', function(){
		return this.get('sortedShops').slice(0, this.get('limit'));
	}),

	hiddenShopsLength: computed('limit', 'sortedShops.length', function(){
		var length = this.get('sortedShops.length') - this.get('limit');
		return length > 0 ? length : false;
	}),

	isGeopositionResolved: computed('currentPosition', function(){
		return typeof this.get('currentPosition') === 'object';
	}),

	documentTitle: computed(function(){
		let firstObject = this.get('model.firstObject');
		if(firstObject){
			return `Vyhledavani autoservisu znacky '${firstObject.get('brand')}' ve meste: '${firstObject.get('city')}'`;
		}
	}),


	markers: computed('visibleMarkers.[]', function(){
		var markers = [];
		this.get('visibleMarkers').forEach(repairShop => {

			var marker = repairShop.get('marker');

			if(!marker.get('content')){
				var markerData = repairShop.getProperties('latitude','longitude');
				markerData.repairShop = repairShop;
				marker = this.store.createRecord('marker', markerData);

				if(this.get('currentPosition')){
					var distance = this.calculateDistance(marker, this.get('currentPosition'));
					distance = Math.round(distance);
					marker.set('distance', distance);
				}
			}

			Ember.run.schedule('afterRender', () => {
				
			    let windowContent = `<strong>${marker.get('repairShop.name')}</strong> <br> 
                         ${marker.get('repairShop.address')} <br>
						<a data-link-${marker.get('repairShop.id')} href="./shop/${marker.get('repairShop.id')}">Detail</a>  
			                         `;
			    var infowindow = new google.maps.InfoWindow({
			    	content: windowContent
			    });

		        marker.get('markerObject').addListener('click', () => {

			    	infowindow.open(marker.get('googleMapObject'), marker.get('markerObject'));
			    	let linkEl = $('[data-link-' + marker.get('repairShop.id') + ']');

			    	linkEl.click( event => {
			    		
			    		if(event.which === 1){
				    		this.transitionToRoute('shop', marker.get('repairShop.id'));
				    		event.preventDefault();
			    		}
			    	});
			    });
			});



			markers.push(marker);
		});



		return markers;
	}),

	deg2rad(deg){
		return deg * (Math.PI/180);
	},

	calculateDistance(marker, currentPosition) {

		var lat1 = marker.get('latitude');
		var lon1 = marker.get('longitude');

		var hasAllowedPosition = currentPosition.coords;

		if(hasAllowedPosition){
		var lat2 = currentPosition.coords.latitude;
		var lon2 = currentPosition.coords.longitude;

		var R = 6371; // Radius of the earth in km
		var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
		var dLon = this.deg2rad(lon2-lon1); 
		var a = 
		Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
		Math.sin(dLon/2) * Math.sin(dLon/2)
		; 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c; // Distance in km
		return d;
		} else {
			return null;
		}	
	},

	actions: {
		initLinks(){
		
		},

		showMoreShops(){
			this.incrementProperty('limit', 5);
		}
	}


});
