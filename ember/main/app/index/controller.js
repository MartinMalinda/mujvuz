import Ember from 'ember';

const {computed} = Ember;

export default Ember.Controller.extend({
	chosenCity:{label:'Praha',value:'praha'},
	chosenBrand:{label:'BMW',value:'BMW'},
	brandChoices: computed(function(){
		return [{label:'BMW',value:'BMW'},{label:'Aston Martin',value:'aston martin'}];
	}),
	cityChoices: computed(function(){
		return [{label:'Brno',value:'brno'},{label:'Praha',value:'praha'},{label:'Ostrava',value:'ostrava'}];
	}),
	actions: {
		findShop(){
			this.transitionToRoute('list', this.get('chosenCity.value'), this.get('chosenBrand.value'));
		}
	}
});
