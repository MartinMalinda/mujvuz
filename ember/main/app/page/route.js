import Ember from 'ember';

export default Ember.Route.extend({
	model(params){
		return this.store.query('page', {orderBy: 'subject', startAt: params.page_subject, endAt: params.page_subject});
	}
});
