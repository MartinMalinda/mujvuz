import Ember from 'ember';
import RouteMetaMixin from 'ember-cli-meta-tags/mixins/route-meta';
import ResetScroll from 'ember-firestarter/mixins/reset-scroll';


export default Ember.Route.extend(RouteMetaMixin,ResetScroll, {
	model(params){
		var shopId = params.shop_id;
		return this.store.find('repairShop', shopId);
	},

	afterModel() {
		this.setHeadTags();
	},

	setHeadTags() {
		var headTags = [{
			type: 'meta',
			tagId: 'meta-description-tag',
			attrs: {
				name: 'description',
				content: 'TEST'
			}
		}];

		this.set('headTags', headTags);
	}
});
