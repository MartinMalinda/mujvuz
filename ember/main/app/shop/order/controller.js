import Ember from 'ember';

const {$, inject} = Ember;

export default Ember.Controller.extend({
	shop: inject.controller('shop'),
	mailData: {},
	emailSent: false,

	actions: {
		sendOrder(){
			var data = this.get('mailData');
			data.shop = this.get('shop.model').toJSON();
			data.shop.id = this.get('shop.model.id');
			this.set('isLoading', true);

			$.post('/send', data, response => {
				if(response.success){
					this.set('emailSent', true);
					this.set('isLoading', false);
				} else {
					this.set('emailError', true);
					this.set('isLoading', false);
				}
			}).fail(() => {
				this.set('emailError', true);
				this.set('isLoading', false);
			});
		}
	}
});
