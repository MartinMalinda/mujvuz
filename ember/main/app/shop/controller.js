import Ember from 'ember';
import notEmpty from 'ember-firestarter/macros/not-empty';

const {computed} = Ember;

export default Ember.Controller.extend({

	hasImage: notEmpty('model.images.firstObject'),
	
	marker: computed('model', function(){

		var repairShop = this.get('model');

		var marker = repairShop.get('marker');
		var markers = [];

			if(!marker.get('content')){
				var markerData = repairShop.getProperties('latitude','longitude');
				markerData.repairShop = repairShop;
				marker = this.store.createRecord('marker', markerData);
			}

			markers.push(marker);
			return markers;
	})
});
