import Ember from 'ember';

const { computed, on } = Ember;

export default Ember.Controller.extend({
	
    latitude: 47,
    longitude: 47,
    zoom: 8,

    geoposition: computed(function(){
    }),
    
    markers: computed(function(){
        var array  = [];
        array.pushObject(this.store.createRecord('marker', {latitude: 18, longitude:18}));
        array.pushObject(this.store.createRecord('marker', {latitude: 20, longitude:20}));
        array.pushObject(this.store.createRecord('marker', {latitude: 19, longitude:19}));

        return array;
    }),

    autologin: on('init', function() {
       // this.send('login');
       // this.set('geoposition', false);
    navigator.geolocation.getCurrentPosition(position => {
        this.set('geoposition', position);
        this.set('geopositionAllowed', true);
    }, error => {
        this.set('geoposition', error);
        this.set('geopositionAllowed', false);
    });

    }),


    actions: {
     
    }
});
