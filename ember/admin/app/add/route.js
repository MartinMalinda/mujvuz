import Ember from 'ember';
import config from 'ember-firestarter/config/environment';

const {get, set} = Ember;

export default Ember.Route.extend({
	model(params){
		console.log();
		if(params.shop_id === 'new'){
			var extra = this.store.createRecord('extra');
			var shop = this.store.createRecord('repairShop', {extra: extra});
			extra.set('repairShop', shop);
			return shop;
		} else {
			return this.store.find('repairShop', params.shop_id);
		}
	},

	filesUploaded: 0,

	saveShop(){
		// shop.save();
      	console.log('saveShop');
	},



	actions: {
	    uploadImage: function (file) {
	    	console.log('uploadImage');
	    	console.log(file);

	   	var shop = this.controller.get('model');

	      var image = this.store.createRecord('image', {
	        shop: shop,
	        // filesize: Ember.get(file, 'size')
	      });

	      file.set('name', image.id + '.jpg');

	      file.read().then(function (url) {
	        if (get(image, 'url') == null) {
	          set(image, 'url', url);
	        }
	      });


	      file.upload(config.backendPath + '/upload').then(function (response) {
	      	console.log(response);
	        set(image, 'filename', response.body[0].fd);
	        image.set('ready', true);
	        console.log('saveImage');
	        return image.save();
	      }, function (response) {
	      	console.log(response);
	        image.rollbackAttributes();
	      }).then(() => {

	      	this.incrementProperty('filesUploaded');
	      	if(this.get('filesUploaded') === file.uploader.files.length){
	      		shop.save();
	      	}

	      });
	    }
  }
});
