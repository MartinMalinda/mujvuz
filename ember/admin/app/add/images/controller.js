import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		setMainImage(id){
			var model = this.get('model');
			model.set('mainImageId', id);
			model.save();

		}
	}
});
