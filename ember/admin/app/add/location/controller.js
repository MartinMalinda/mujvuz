import Ember from 'ember';

export default Ember.Controller.extend({

	canGetCoordinates: Ember.computed.notEmpty('model.address'),

	marker: Ember.computed('model.id', function(){
		let marker = this.get('model.marker');
		if(Ember.isPresent(marker)){
			return [this.get('model.marker')];
		} else {
			return null;
		}
	}),
	
	actions: {
		getCoordinates(){
			console.log(this.get('model.address'));
			let address = this.get('model.address');
			let geocoder = new google.maps.Geocoder();
			geocoder.geocode({'address': address}, (results, status) => {
				console.log(results);
				console.log(status);
				var location = results[0].geometry.location;
				let oldMarker = this.get('model.marker');
				if(oldMarker && oldMarker.get('latitude')){

					// remove old marker from the map and from the store
					oldMarker.get('markerObject').setMap(null);
					oldMarker.deleteRecord();
				}
				var newMarker = this.store.createRecord('marker', {draggable: true, latitude: location.lat(), longitude: location.lng()});
				this.set('model.marker', newMarker);
				this.set('model.latitude', this.get('model.marker.latitude'));
				this.set('model.longitude', this.get('model.marker.longitude'));
				this.set('marker', [newMarker]);
			});
		}
	}
});
