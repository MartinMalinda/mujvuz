import Ember from 'ember';

export default Ember.Controller.extend({


actions: {

 	addShop(){
		
		this.set('model.cityBrandIndex', `${this.get('model.city')}_${this.get('model.brand')}`);
		this.set('model.latitude', this.get('model.marker.latitude'));
		this.set('model.longitude', this.get('model.marker.longitude'));

		this.get('model').save().then(() => {
			var extra = this.store.peekRecord('extra', this.get('model.extra.id'));
			return extra.save();
			// return this.get('model.extra').save();
		}).then(() => {
			var company = this.store.peekRecord('company', this.get('model.company.id'));
			return company.save();
		}).then(() => {
			this.transitionToRoute('index');
			this.set('model', {});
		});
  	},

  	setVisibility(bool){


  		this.set('model.isVisible', bool === 'true');
  	}
}


});
