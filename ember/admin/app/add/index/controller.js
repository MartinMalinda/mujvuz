import Ember from 'ember';
import config from 'ember-firestarter/config/environment';

const {computed} = Ember;

export default Ember.Controller.extend({

	brandChoices: computed(function(){
		return [{label:'BMW',value:'BMW'},{label:'Aston Martin',value:'Aston Martin'}];
	}),
	cityChoices: computed(function(){
		return [{label:'Brno',value:'brno'},{label:'Praha',value:'praha'},{label:'Ostrava',value:'ostrava'}];
	}),

	companies: computed(function(){
		return this.store.findAll('company');
	}),

	// states


	actions: {

		selectCity(component, id, value) {
        	this.set('model.city', id);
      	},
		selectBrand(component, id, value) {
        	this.set('model.brand', id);
      	},
 
      	updateOpeningHours(day, from, to){
      		// console.log('setting opening hours', from, to, day);
      		this.set('model.'+ day + 'Start',from);
      		this.set('model.'+ day + 'End',to);

      	}

	}
});
