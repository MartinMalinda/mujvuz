import DS from 'ember-data';
import Ember from 'ember';

const { computed, isEmpty } = Ember;
const { attr, belongsTo, hasMany } = DS;

export default DS.Model.extend({

  name: attr('string'),
  address: attr('string'),
  brand: attr('string'),
  city: attr('string', {defaultValue: "praha"}),

  info: attr('string'),

  image: attr('string'),

  phone: attr('string'),
  url: attr('string'),
  email: attr('string'),
  mainImageId: attr('string'),
  

  isVisible: attr('boolean', {defaultValue: false}),

  announcement: attr('string'),

  mondayStart: attr('string', {defaultValue: "08:00"}),
  tuesdayStart: attr('string', {defaultValue: "08:00"}),
  wednesdayStart: attr('string', {defaultValue: "08:00"}),
  thursdayStart: attr('string', {defaultValue: "08:00"}),
  fridayStart: attr('string', {defaultValue: "08:00"}),
  saturdayStart: attr('string', {defaultValue: "08:00"}),
  sundayStart: attr('string', {defaultValue: "08:00"}),

  mondayEnd: attr('string', {defaultValue: "21:00"}),
  tuesdayEnd: attr('string', {defaultValue: "21:00"}),
  wednesdayEnd: attr('string', {defaultValue: "21:00"}),
  thursdayEnd: attr('string', {defaultValue: "21:00"}),
  fridayEnd: attr('string', {defaultValue: "21:00"}),
  saturdayEnd: attr('string', {defaultValue: "21:00"}),
  sundayEnd: attr('string', {defaultValue: "21:00"}),

  minElectricPrice: attr('number', {defaultValue: 0}),
  minMechPrice: attr('number', {defaultValue: 0}),
  minKlempPrice: attr('number', {defaultValue: 0}),
  minLakPrice: attr('number', {defaultValue: 0}),

  maxElectricPrice: attr('number', {defaultValue: 0}),
  maxMechPrice: attr('number', {defaultValue: 0}),
  maxKlempPrice: attr('number', {defaultValue: 0}),
  maxLakPrice: attr('number', {defaultValue: 0}),

  cityBrandIndex: attr('string', {defaultValue: 'undefined_undefined'}),

  latitude: attr('number'),
  longitude: attr('number'),

  extra: belongsTo('extra'),
  company: belongsTo('company'),
  images: hasMany('image'),

  hasICO: computed.notEmpty('company.ico'),

  mainImage: computed('mainImageId', function(){
    var id = this.get('mainImageId');
    if(!isEmpty(id)){
      return this.get('images').findBy('id', id);
    } else {
      return this.get('images.firstObject');
    }
  }),

  // marker: DS.attr(),

  ready(){
      
    if(Ember.isPresent(this.get('latitude'))){
      var marker = this.store.createRecord('marker', {
        draggable: true,
        latitude: this.get('latitude'),
        longitude: this.get('longitude')
      });
      this.set('marker', marker);
      // this.set('marker',[marker]);
    }
 
  },

});
