import DS from 'ember-data';
import Ember from 'ember';
import config from 'ember-firestarter/config/environment';

const {attr, belongsTo} = DS;
const {computed} = Ember;

export default DS.Model.extend({

  filename: attr('string'),
  shop: belongsTo('repairShop'),
  note: attr('string'),
  ready: attr('boolean'),
  fullUrl: computed('filename',function(){
  	return 'https://s3-eu-west-1.amazonaws.com/mujvuz2/' + this.get('filename');
  })

});
