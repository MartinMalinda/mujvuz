import DS from 'ember-data';

const { attr, belongsTo } = DS;

export default DS.Model.extend({

  repairShop: belongsTo('repairShop'),
  ico: attr('number'),
  note: attr('string'),
  email: attr('string')

});
