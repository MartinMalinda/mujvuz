import DS from 'ember-data';
import Ember from 'ember';

const {attr, belongsTo} = DS;
const {computed, observer} = Ember;

export default DS.Model.extend({
  latitude: attr('number'),
  longitude: attr('number'),

  repairShop: belongsTo('repairShop'),

  markerObject: computed('googleMapObject', function(){
    var position = new google.maps.LatLng(this.get('latitude'),this.get('longitude'));
    var draggable = this.get('draggable') || false;
    var marker = new google.maps.Marker({
      position: position,
      title: 'test',
      draggable: draggable
    });
    marker.id = this.get('id');

    if(draggable){
      google.maps.event.addListener(marker, 'dragend', () => {
        var newPosition = marker.getPosition();
        this.set('latitude', newPosition.lat());
        this.set('longitude', newPosition.lng());
      });
    }

    return marker;
  }),

  markerInserter: observer('googleMapObject', function(){
  	
    var marker = this.get('markerObject');
  	marker.setMap(this.get('googleMapObject'));
  	// console.log(marker);
  	// return marker;
  }).on('init')
});
