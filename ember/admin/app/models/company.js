import DS from 'ember-data';

const {attr, belongsTo, hasMany} = DS;
const {computed} = Ember;

export default DS.Model.extend({
	
  name: attr('string'),
  ico: attr('number'),
  repairShops: hasMany('repairShop'),
  note: attr('string'),

  hasICO: computed.notEmpty('ico'),

  
});
