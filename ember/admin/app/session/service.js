import Ember from 'ember';
import SessionService from 'ember-simple-auth/services/session'

export default SessionService.extend({
	init(){
		console.log('test');
		this._super();
	},
	uid: Ember.computed.alias('session.content.authenticated.uid').readOnly()
});
