import Ember from 'ember';

export default Ember.Controller.extend({
	isLoading: false,

	actions: {
		addPage(){
		
			this.store.createRecord('page');
		},
		deletePage(page){
			if(confirm('Opravdu smazat?')){
				page.destroyRecord().then(() => {
					this.transitionToRoute('pages.index');
				});
			}
		}
	}
});
