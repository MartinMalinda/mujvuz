import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('user', function() {
    this.route('signup');
    this.route('login');
  });
  // this.route('shop', {path: '/shop/:shop_id'});
  this.route('add', {path: '/add/:shop_id'}, function() {
    this.route('images');
    this.route('location');
    this.route('action');
  });

  this.route('index', {path: '/'}, function() {
    this.route('list', {path: '/list/:city/:brand'});
  });
  this.route('pages', function() {
    this.route('detail', {path: '/:page_id'});
  });
  this.route('company', function() {
    this.route('detail', {path: '/:company_id'});
  });
});

export default Router;
