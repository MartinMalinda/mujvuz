import Ember from 'ember';

export default Ember.Controller.extend({

 
    email: 'malindacz@gmail.com',
    password: 'somePassword',
    loading: false,

	session: Ember.inject.service('session'),
	currentUser: Ember.computed('session.uid', function(){
		return this.store.find('user', this.get('session.uid'));
	}),

    autologin: Ember.on('init', function() {
       // this.send('login');
    navigator.geolocation.getCurrentPosition(position => {
        this.set('geoposition', position);
        console.log(position);
    });

    }),

    geoposition: Ember.computed(function(){
    }),

    actions: {
        login: function() {
            this.set('loading', true);
            this.get('session').authenticate('authenticator:firebase', {
                'email': this.get('email'),
                'password': this.get('password')
            }).then(() => {
                // this.transitionToRoute('index');
                this.set('loading', false);
                
            });

        },
        logout: function() {
            this.get('session').invalidate().then(function() {
                this.transitionToRoute('login');
            }.bind(this));
        }
    }
});
