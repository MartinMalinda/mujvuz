import Ember from 'ember';

const {on, $} = Ember;

export default Ember.Component.extend({
	classNames: 'application',
	stopTheSpinner: on('didInsertElement', function(){
		$('body').removeClass('au-loading');
	})
});
