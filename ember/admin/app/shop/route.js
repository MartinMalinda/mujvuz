import Ember from 'ember';

export default Ember.Route.extend({
	model(params){
		var shopId = params.shop_id;
		return this.store.find('repairShop', shopId);
	}
});
