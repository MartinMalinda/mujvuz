import Ember from 'ember';

const {computed} = Ember;

export default Ember.Controller.extend({
	
	marker: computed('model', function(){

		var repairShop = this.get('model');

		var marker = repairShop.get('marker');
		var markers = [];
			console.log(marker);

			if(!marker.get('content')){
				var markerData = repairShop.getProperties('latitude','longitude');
				markerData.repairShop = repairShop;
				marker = this.store.createRecord('marker', markerData);
			}

			markers.push(marker);
			return markers;
	})
});
