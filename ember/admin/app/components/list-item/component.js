import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['item'],
	isEditing: false,

	actions: {
		toggleEdit(){
			this.toggleProperty('isEditing');
		}
	}
});
