import Ember from 'ember';
import config from 'ember-firestarter/config/environment';

const {$, computed} = Ember;

export default Ember.Component.extend({

	classNames: ['ui', 'card'],
	classNameBindings: ['loading:ui-loading'],

	displayRibbon: computed('model.id', 'mainImageId', function(){
		return this.get('model.id') === this.get('mainImageId');
	}),

	didInsertElement(){
		// this.get('model').then(() => {
			const image = this.$().find('.image');
			console.log(image);
			image.dimmer({
			  on: 'hover'
			});
		// });
	},
	actions: {
		delete(){
			//get some secret here
			this.set('loading', true);
			$.post(config.backendPath + '/delete', {name: this.get('model.filename')}).then(() => {
				this.get('model').destroyRecord();
				// this.set('')
				// this.set('loading')
			});
		}
	}
});
