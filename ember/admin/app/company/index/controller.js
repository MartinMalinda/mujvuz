import Ember from 'ember';

const {computed} = Ember;

export default Ember.Controller.extend({
	isLoading: false,
	ico: null,

	filteringByICO: computed.notEmpty('ico'),

	filteredCompanies: computed.filter('model', function(company){

		if(this.get('filteringByICO')){
			
			if(company.get('hasICO')){
				let ico = this.get('ico');
				return ico === company.get('ico').toString().substring(0, ico.length);
			}
			return false;
		} else {
			
			return true;

		}
	}),

	actions: {

		addCompany(){
			var newCompany = this.store.createRecord('company');
			this.transitionToRoute('company.detail', newCompany);
		},

		delete(model){
			if(confirm('Opravdu smazat?')){
				this.set('isDeleting', true);
				model.destroyRecord().then(() => {
					this.set('isDeleting', false);
				});
			}
		},

		filterCompanies(){
			this.notifyPropertyChange('filteredCompanies');
		}
	},

});
