import Ember from 'ember';

const {computed} = Ember;

export default Ember.Controller.extend({

	selectedBrand: 'aston martin',
	selectedCity: 'brno',
	ico: null,

	filteringByICO: computed.notEmpty('ico'),

	dropdownClass: computed('filteringByICO', function(){
		if(this.get('filteringByICO')){
			return 'selection disabled';
		} else {
			return 'selection';
		}
	}),

	brandChoices: computed(function(){
		return [{label:'BMW',value:'BMW'},
				{label:'Aston Martin',value:'Aston Martin'},
				{label:'Nezarazene',value:'nezarazene'}];
	}),
	cityChoices: computed(function(){
		return [{label:'Brno',value:'brno'},
				{label:'Praha',value:'praha'},
				{label:'Ostrava',value:'ostrava'},
				{label:'Nezarazene',value:'nezarazene'}];
	}),

	filteredShops: computed.filter('model', function(shop){
		if(this.get('filteringByICO')){
			if(shop.get('hasICO')){
				let ico = this.get('ico');
				return ico === shop.get('company.ico').toString().substring(0, ico.length);
			}
			return false;
		} else {
			let brandMatches = shop.get('brand') === this.get('selectedBrand');
			let cityMatches = shop.get('city') === this.get('selectedCity');
			return brandMatches && cityMatches;
		}
	}),

	updateSelection(){

	},
	actions: {
		deleteShop(shop){
			if(confirm("Opravdu smazat?")){
				shop.destroyRecord();
			}
		},
		
		filterShops(){
			this.notifyPropertyChange('filteredShops');
		}
	}
});
