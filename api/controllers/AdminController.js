/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// var fs = require('fs');
var knox = require('knox'); 


module.exports = {

	serveAsset: function(req, res){
		var path = req.path.substr(9, req.path.length);
		res.sendfile('./ember/admin/dist'+path);
	},

	serveEmber: function(req, res){
		res.sendfile('./ember/admin/dist/index.html');
	},

	upload: function(req, res){

		var file = req.file('file');
		
		console.log(req.allParams());
			// dirname: sails.config.appPath + '/upload/images',
			// saveAs: req.param('name')
			// console.log(process.env.S3ACCESSKEY);
		file.upload({
			adapter: require('skipper-s3'),
			key: process.env.S3ACCESSKEY,
			secret: process.env.S3SECRETKEY,
			bucket: 'mujvuz2',
			region: 'eu-west-1'
		}, function(err, uploadedFiles){
			if(err) return res.send(500, err);
			return res.send(200, uploadedFiles);
		});
	},

	deleteImage: function(req, res){
		var imgPath = req.param('name');
		// fs.unlink(sails.config.appPath + '/upload/images/' + imgId + '.jpg', function(err){
			// if(err) {
			// 	console.log(err);
			// 	return res.send(500, err);
			// }
			// return res.send(200, {deleted: imgId});
		// });
		var client = knox.createClient({
		    key: process.env.S3ACCESSKEY,
		    secret: process.env.S3SECRETKEY,
		    bucket: 'mujvuz2',
		    region: 'eu-west-1'
		});
		console.log(imgPath);

		client.deleteFile(imgPath, function(err, knoxRes){
		    // console.log('Clip delete err :',err,', Clip path :',imgPath);
		    if(err) {
				console.log(err);
				return res.send(500, err);
			}
			return res.send(200, {deleted: imgPath});
		});
	}
};

