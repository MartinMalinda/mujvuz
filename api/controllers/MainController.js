/**
 * MainController
 *
 * @description :: Server-side logic for managing mains
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// var request = require('request');

var Firebase = require('firebase');

module.exports = {

	serveAsset: function(req, res){
		res.sendfile('./ember/main/dist'+req.path);
	},

	serveImage: function(req, res){
		console.log('./'+req.path);
		res.sendfile('./'+req.path);
	},

	serveEmber: function(req, res){
		
		res.sendfile('./ember/main/dist/index.html');
	},

	sendEmail: function(req, res){

		var data = {
			senderName: req.param('name'),
			senderEmail: req.param('email'),
			note: req.param('note'),
			phone: req.param('phone'),
			shop: req.param('shop').name
		};

		var options = {
			to: req.param('shop').email,
			subject: 'MujVuz.cz - Kontaktni formular | ' + req.param('shop').name,
			bcc: 'malindacz@gmail.com',
			replyTo: req.param('email')
		};

		var ref = sails.hooks.firebase.ref;
		ref.child('extras/' + req.param('shop').extra).once('value', function(snapshot){

			var extra = snapshot.val();
			var email = extra.email ? extra.email : req.param('shop').email;

			options.to = email;
			sails.hooks.email.send('contact', data, options, function(err){
				if(err) return res.negotiate(err);
				return res.json({success: true});
			});

		}, function(err){
			return res.negotiate(err);
		});


		// sails.hooks.email.send('contact',data,
		// options,
		// function(err) {
		// 	if(err){
		// 		return res.negotiate(err);
		// 	}
		// 	return res.json({success: true});
		// });
	}
};

