module.exports.email = {
	service: 'SES',
	auth: {
		user: process.env.SMTPUSER,
		pass: process.env.SMTPPWD
	},
	region: 'eu-west-1',
	from: 'malindacz@gmail.com',
	alwaysSendTo: 'malindacz@gmail.com',
	testMode: false //DEV
};